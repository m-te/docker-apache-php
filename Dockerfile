FROM debian:stretch
MAINTAINER Marc Teinturier <marc.teinturier@gmail.com>

EXPOSE 80

# Declaration des uid et gid par défaut pour Apache
ENV USER_ID 930
ENV GROUP_ID 930
ENV APACHE_UMASK 002
ENV TIMEZONE Europe/Paris

ENV SSMTP_ROOT root@dev-php72.local
ENV SSMTP_HUB smtp_catchall:25
ENV SSMTP_HOSTNAME dev-php72.local

RUN mkdir -p /opt/bin

RUN apt-get -y update && apt-get install -y \
apt-transport-https \
lsb-release \
ca-certificates \
wget

RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
RUN sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'

RUN apt-get -y update && apt-get install -y \
apache2 \
mysql-client \
redis-tools \
git \
vim \
imagemagick \
dnsutils \
iputils-ping \
curl \
ssmtp \
ftp \
unzip \
jpegoptim \
mailutils \
php7.2 \
php7.2-cli \
php-redis \
php7.2-mysql \
php7.2-curl \
php7.2-gd \
php7.2-intl \
php7.2-xsl \
php-mcrypt \
php7.2-zip \
php7.2-xdebug \
php-memcached \
php7.2-mbstring \
php-xml \
php-libsodium \
php7.2-soap \
php7.2-json \
php-imagick \
php-xdebug \
poppler-utils \
memdump \
openjdk-8-jre && apt-get clean autoclean && apt-get autoremove --yes && rm -rf /var/lib/{apt,dpkg,cache,log}/

COPY AtiwebCA.crt /usr/local/share/ca-certificates/

# Configuration du timezone
RUN echo "${TIMEZONE}" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

# Ajout du CA DDTH
RUN update-ca-certificates

# PHPUnit
RUN ["/usr/bin/wget", "-O", "/usr/local/bin/phpunit", "https://phar.phpunit.de/phpunit-6.2.phar"]
RUN ["/bin/chmod", "+x", "/usr/local/bin/phpunit"]

# Composer
RUN ["/usr/bin/wget", "-O", "/root/composer-setup.php", "https://getcomposer.org/installer"]
RUN ["/usr/bin/php", "/root/composer-setup.php", "--install-dir=/usr/local/bin"]
RUN ["/bin/rm", "/root/composer-setup.php"]
# RUN ["/bin/mv", "/usr/local/bin/composer.phar", "/usr/local/bin/composer"]
# RUN ["/bin/chmod", "+x", "/usr/local/bin/composer"]

# Activation des modules Apache
RUN ["/usr/sbin/a2enmod", "ssl", "rewrite", "proxy", "proxy_http", "headers", "remoteip", "expires", "mime", "mime_magic"]

# Ajout du user pour Apache avec le même uid et gid que sur le système
RUN groupadd -g ${GROUP_ID} apache
RUN useradd --system --shell /bin/bash -g ${GROUP_ID} -u ${USER_ID} -G adm -d /home/apache apache
RUN mkdir -p /home/apache
RUN chown -R apache:apache /home/apache
RUN sed -i 's/www-data/apache/g' /etc/apache2/envvars

COPY bashrc /home/apache/.bashrc
COPY bashrc /root/.bashrc

RUN echo "umask ${APACHE_UMASK}" >> /home/apache/.bashrc
RUN echo "umask ${APACHE_UMASK}" >> /root/.bashrc

COPY entrypoint.sh /opt/bin/entrypoint.sh
RUN /bin/chmod +x /opt/bin/*

RUN wget https://get.symfony.com/cli/installer -O - | bash

RUN cp "/usr/local/bin/composer.phar" "/usr/local/bin/composer"
RUN ["/bin/chmod", "+x", "/usr/local/bin/composer"]
RUN rm "/usr/local/bin/composer.phar"

WORKDIR "/var/www"

# Démarrage d'Apache
ENTRYPOINT ["/bin/bash", "/opt/bin/entrypoint.sh"]
