# !/bin/bash
echo "Setting UID: $USER_ID and GID $GROUP_ID"
sed -i "s/930:930/$USER_ID:$GROUP_ID/" /etc/passwd
sed -i "s/930/$GROUP_ID/" /etc/group
chown -R apache:apache /home/apache

echo "Config ssmtp"
echo "root=$SSMTP_ROOT" > /etc/ssmtp/ssmtp.conf
echo "mailhub=$SSMTP_HUB" >> /etc/ssmtp/ssmtp.conf
echo "hostname=$SSMTP_HOSTNAME" >> /etc/ssmtp/ssmtp.conf

echo "----------------------------------------------"
echo "Content of /var/log/apache2/*"
cat /var/log/apache2/*
echo "----------------------------------------------"

echo "Starting apache"
# Apache gets grumpy about PID files pre-existing
rm -f /var/run/apache2/apache2.pid
umask $APACHE_UMASK
/usr/sbin/apache2ctl -DFOREGROUND
